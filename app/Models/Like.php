<?php
/**
 * Created by PhpStorm.
 * User: Iulian
 * Date: 2/5/2019
 * Time: 8:10 PM
 */

namespace App\Models;



class Like
{
    public static function getLikeNumber($pdo, $idPost)
    {
        $query = "SELECT COUNT(likes.postId) FROM `likes` WHERE likes.postId=\"$idPost\"";
                  
        $stmt = $pdo->prepare($query);
        $stmt->execute([]);
        $result =  $stmt->fetchColumn();
        return $result;
    }


    public static function addLike($pdo, $idPost, $username)
    {
        try {
            if(Post::getPost($pdo,$idPost)!=NULL) {
                $query = "SELECT COUNT(likes.userId) FROM likes WHERE likes.postId=\"$idPost\" AND likes.userId=
        (SELECT users.id FROM `users` WHERE users.username=\"$username\")";
                $stmt = $pdo->prepare($query);
                $stmt->execute([]);
                $result = $stmt->fetchColumn();

                if ($result < 1) {

                    $query = "INSERT INTO `likes` ( `postId`, `userId`) VALUES ( ?, (SELECT users.id FROM `users` WHERE users.username=?))";


                    $stmt = $pdo->prepare($query);
                    $stmt->execute([$idPost, $username]);

                }
            }
        }
        catch (Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }
    }
}
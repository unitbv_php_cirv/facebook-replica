<?php

namespace App\Models;

use Framework\FileOperator;

class Post
{

    public static function createPost($pdo, $username, $description, $id = 0)
    {
        try {
            if($id==0 OR Post::getPost($pdo,$id)!=NULL) {

                $dateTime = date("Y-m-d H:i:s");
                $propozitie = 'insert into `posts` (username, description, dateTime, pictureLink, postId) values (?, ?, ?, ?, ?)';
                if ($id == 0) {
                    $fileInputName = "fileToUpload";
                } else {
                    $fileInputName = "commentToUpload";
                }
                if ($_FILES[$fileInputName]["name"]) {
                    $fileLocation = (new FileOperator())->UploadFile($fileInputName);
                    if (isset($fileLocation)) {
                        $pdo->prepare($propozitie)->execute([$username, $description, $dateTime, $fileLocation, $id]);
                    } else {
                        return;
                    }
                } else {
                    $pdo->prepare($propozitie)->execute([$username, $description, $dateTime, null, $id]);
                }
            }
        }
        catch (Exception $e)
        {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
        }

    }

    public static function deletePost($pdo, $id)
    {
        $statement = "DELETE FROM `posts` WHERE `id` = \"$id\" OR `postId` = \"$id\"";
        $pdo->prepare($statement)->execute([]);
    }

    public static function getPosts($pdo, string $username = '')
    {
        $whereStmt = '';
        if ($username) {
            $whereStmt = " WHERE posts.username = \"$username\" AND posts.postId = 0 ";
        }
        else
        {
            $whereStmt = " WHERE posts.postId = 0 ";
        }

        $query = "SELECT posts.id, posts.username, posts.description, posts.dateTime, posts.pictureLink, users.firstName, users.lastName, users.profileImageLink
                  FROM posts `posts`
                  INNER JOIN users `users` ON posts.username = users.username "
                  .$whereStmt.
                  "ORDER BY posts.dateTime DESC";
        $stmt = $pdo->prepare($query);
        $stmt->execute([]);
        $result =  $stmt->fetchAll();

        return $result;
    }

    public static function getPost($pdo, $idPost)
    {
        $query = "SELECT id FROM `posts` WHERE id = \"$idPost\"";
        $stmt = $pdo->prepare($query);
        $stmt->execute([]);
        $result =  $stmt->fetchAll();
        return $result;
    }

    public static function getPostComments($pdo, $idPost)
    {
        $query = "SELECT posts.id, posts.username, posts.description, posts.dateTime, posts.pictureLink, users.firstName, users.lastName, users.profileImageLink
                  FROM posts `posts`
                  INNER JOIN users `users` ON posts.username = users.username
                  WHERE posts.postId = \"$idPost\"
                  ORDER BY posts.dateTime DESC";
        $stmt = $pdo->prepare($query);
        $stmt->execute([]);
        $result =  $stmt->fetchAll();
        return $result;
    }

    public static function editPost($pdo, $id)
    {
        $querry = "UPDATE `posts`
                   SET description=?
                   WHERE id=?";

        $pdo->prepare($querry)->execute([$_POST['newDescription'], $id]);
    }
}

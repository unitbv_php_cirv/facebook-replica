<?php

namespace App\Models;

class User
{
    public static function createUser($pdo, $username, $password, $email, $firstName, $lastName, $profileImageLink)
    {
        $propozitie = 'insert into `users` (username, password, email, firstName, lastName, profileImageLink) values (?, ?, ?, ?, ?, ?)';

        $stmt = $pdo->prepare($propozitie);
        $result = $stmt->execute([$username, password_hash($password, PASSWORD_DEFAULT), $email, $firstName, $lastName, $profileImageLink]);
        return $result;
    }

    public static function updateUserDetails($pdo, $username, $firstName, $lastName, $profileImageLink)
    {
        if($profileImageLink == null) {
            $propozitie = "UPDATE `users` SET firstName = \"$firstName\", lastName = \"$lastName\" WHERE username = \"$username\"";
        }
        else{
            $propozitie = "UPDATE `users` SET firstName = \"$firstName\", lastName = \"$lastName\", profileImageLink = \"$profileImageLink\" WHERE username = \"$username\"";
        }
        $stmt = $pdo->prepare($propozitie);
        $result = $stmt->execute([]);
        return $result;
    }

    public static function updateUserPassword($pdo, $username, $password)
    {
        $passHash = password_hash($password, PASSWORD_DEFAULT);
        $propozitie = "UPDATE `users` SET password = \"$passHash\" WHERE username = \"$username\"";
        $stmt = $pdo->prepare($propozitie);
        $result = $stmt->execute([]);
        return $result;
    }

    public static function getUserByEmail($pdo, $email)
    {
        $propozitie = "select * from `users` where email = \"$email\"";
        $stmt = $pdo->prepare($propozitie);
        $stmt->execute([]);
        $result =  $stmt->fetch();
        return $result;
    }

    public static function getUserByUsername($pdo, $username)
    {
        $propozitie = "select * from `users` where username = \"$username\"";
        $stmt = $pdo->prepare($propozitie);
        $stmt->execute([]);
        $result =  $stmt->fetch();
        return $result;
    }

    public static function checkUserExists($pdo, $username)
    {
        $propozitie = 'SELECT * FROM `users` WHERE `username` = ?';
        $statement = $pdo->prepare($propozitie);
        $statement->execute([$username]);
        $rows = $statement->fetch();

        if ($rows)
        {
            return true;
        }

        return false;
    }

    public static function deleteUser($pdo, $username)
    {
        $propozitie = "delete from `users` where username = \"$username\"";
        $stmt = $pdo->prepare($propozitie);
        $result = $stmt->execute([]);
        $propozitiePosts = "delete from `posts` where username = \"$username\"";
        $stmt = $pdo->prepare($propozitiePosts);
        $result = $stmt->execute([]);
        return $result;
    }
}

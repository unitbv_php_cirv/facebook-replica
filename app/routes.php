<?php
$static_routes = [
    ''                      => ['controller' => 'NotFoundController',
                                'action'     => 'notFoundAction'],
    '/'                     => ['controller' => 'IndexController',
                                'action'     => 'indexAction',
                                'middleware' => 'Authenticated'],
    '/login/auth/'          => ['controller' => 'LogInController',
                                'action'     => 'logIn',
                                'middleware' => 'Authenticated'],
    '/register/'            => ['controller' => 'RegisterController',
                                'action'     => 'register',
                                'middleware' => 'Authenticated'],
    '/logout/'              => ['controller' => 'LogOutController',
                                'action'     => 'logOut',
                                'middleware' => 'NotAuthenticated'],
    '/wall/'                => ['controller' => 'WallController',
                                'action'     => 'Show',
                                'middleware' => 'NotAuthenticated'],
    '/settings/'            => ['controller' => 'SettingsController',
                                'action'     => 'Show',
                                'middleware' => 'NotAuthenticated']
];

$dynamic_routes = [
    '/userProfile/{user}/'      => ['controller' => 'UserProfileController',
                                    'action'     => 'Show',
                                    'middleware' => 'NotAuthenticated'],
    '/post/{id}/'               => ['controller' => 'GetPostController',
                                    'action'     => 'getPost',
                                    'middleware' => 'NotAuthenticated']
];

<?php

namespace App\Helpers;

use App\Models\Post;
use App\Models\Like;

class PostHelper
{
    public function AddNewPost($pdo)
    {
        $description = $_POST["description"];
        $user = $_SESSION["username"];
        if($description != "" || $_FILES["fileToUpload"]["name"] != "") {
            $postModel = new Post();
            if(isset($_POST["isComment"])) {
                $postModel->createPost($pdo, $user, $description, $_POST["isComment"]);
            }
            else {
                $postModel->createPost($pdo, $user, $description);
            }
            unset($_POST["addPost"]);
            header("Location: " . $_SESSION["currentPage"]);
        }
    }


    public function DeletePost($pdo, $id)
    {
        $postModel = new Post();
        $postModel->deletePost($pdo, $id);
        unset($_POST["deletePost"]);
    }

    public function ConfigurePost($pdo, $post)
    {
        $postModel = new Post();
        $post["userNameAndSurname"] = $post["firstName"]." ".$post["lastName"];
        $post["userLink"] = "/userProfile/".$post["username"];
        $post["comments"] = $postModel->getPostComments($pdo, $post["id"]);
        for ($j = 0; $j < sizeof($post["comments"]); $j++) {
            $post["comments"][$j]["userNameAndSurname"] = $post["comments"][$j]["firstName"]." ".$post["comments"][$j]["lastName"];
            $post["comments"][$j]["userLink"] = "/userProfile/".$post["comments"][$j]["username"];
        }
        return $post;
    }

    public function EditPost($pdo, $id) {
        $postModel = new Post();
        $postModel->editPost($pdo, $id);
        unset($_POST["editPost"]);
    }

    public function UpdateLikeNumber($pdo, $idPost, $username)
    {
        $likeModel = new Like();
        $likeModel->addLike($pdo, $idPost, $username);
        unset($_POST["updateLike"]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: vladv
 * Date: 05-Feb-19
 * Time: 9:14 PM
 */

namespace App\Helpers;


class ErrorTrigger
{

    public static function add(&$twigParams)
    {
        session_start();
        if (isset($_SESSION['errorMsg'])) {
            $msg = $_SESSION['errorMsg'];
            $twigParams['errorMsg'] = $msg;
            unset($_SESSION['errorMsg']);
        }
    }

}
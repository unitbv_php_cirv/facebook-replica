<?php

namespace App\Middlewares;

use Framework\Middleware;


class NotAuthenticated implements Middleware
{
    public function handle(array $params = null)
    {
        session_start();
        if (!isset($_SESSION['username'])){
            $this->reject();
        }
    }

    function reject()
    {
        header("Location: /");
    }
}
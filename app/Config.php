<?php
namespace App;

class EnvironmentType
{
    const PRODUCTION = 0;
    const DEVELOPMENT = 1;
}

class DeploymentType
{
    const LOCAL = 0;
    const AMAZON_AWS_EB = 1;
}

class Config
{
    const ENVIRONMENT = EnvironmentType::DEVELOPMENT;
    const DEPLOYMENT = DeploymentType::AMAZON_AWS_EB;
}

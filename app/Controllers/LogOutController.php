<?php

namespace App\Controllers;

use Framework\Controller;

class LogOutController extends Controller
{

    private function unsetFromSession($key)
    {
        if (isset($_SESSION[$key])) {
            unset($_SESSION[$key]);
        }
    }

    public function logOut()
    {
        session_start();
        $this->unsetFromSession('username');
        $this->unsetFromSession('firstName');
        $this->unsetFromSession('lastName');
        $this->unsetFromSession('profileImageLink');

        header('Location: /');
    }
}
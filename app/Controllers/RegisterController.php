<?php

namespace App\Controllers;

use Framework\Controller;
use Framework\FileOperator;
use App\Models\User;
use App\db;

class RegisterController extends Controller
{

    public function register()
    {
        if (!isset($_POST['firstName'])
            || !isset($_POST['lastName'])
            || !isset($_POST['username'])
            || !isset($_POST['email'])
            || !isset($_POST['password'])
            || !isset($_POST['confirmPassword']))
        {
            $_SESSION['errorMsg'] = 'Error transmitting data.';
            header("Location: /");
            return;
        }

        if ($_POST['password'] !== $_POST['confirmPassword']) {
            $_SESSION['errorMsg'] = "Passwords not equal";
            header("Location: /");
            return;
        }

        if (preg_match('/\s/',$_POST['username'])) {
            $_SESSION['errorMsg'] = "Username must not contain whitespaces";
            header("Location: /");
            return;
        }

        if (strpos($_POST['username'], '@')) {
            $_SESSION['errorMsg'] = "Username must not contain @ character.";
            header("Location: /");
            return;
        }

        $pdo = (new db())->dbConnection();

        $userByEmail = User::getUserByEmail($pdo, $_POST['email']);

        if ($userByEmail) {
            $_SESSION['errorMsg'] = "Email already used by another user.";
            header("Location: /");
            return;
        }

        $userByUsername = User::getUserByUsername($pdo, $_POST['username']);

        if ($userByUsername) {
            $_SESSION['errorMsg'] = "Username already used by another user.";
            header("Location: /");
            return;
        }

        $profileImageLink = "/Pictures/DefaultProfileImage.jpg";
        if($_FILES["profileImageToUpload"]["name"]) {
            $fileLocation = (new FileOperator())->UploadFile("profileImageToUpload");
            if (isset($fileLocation)) {
                $profileImageLink = $fileLocation;
            }
        }

        $success = User::createUser($pdo, $_POST['username'], $_POST['password'], $_POST['email'], $_POST['firstName'], $_POST['lastName'], $profileImageLink);
        if (!$success) {
            $_SESSION['errorMsg'] = "Unable to register the user :(";
            header("Location: /");
            return;
        }
        else {
            session_start();
            $_SESSION['username'] = $_POST['username'];
            $_SESSION['firstName'] = $_POST['firstName'];
            $_SESSION['lastName'] = $_POST['lastName'];
            $_SESSION['profileImageLink'] = $profileImageLink;

            header("Location: /wall/");
        }
    }
}
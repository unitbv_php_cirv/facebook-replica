<?php

namespace App\Controllers;

use Framework\Controller;
use App\Models\User;
use App\db;

class LogInController extends Controller
{

    public function logIn()
    {
        if (!isset($_POST['email']) || !isset($_POST['password'])) {
            $_SESSION['errorMsg'] = 'Error transmitting data.';
            header("Location: /");
            return;
        }

        $inputEmail = $_POST['email'];
        $inputPassword = $_POST['password'];

        $pdo = (new db())->dbConnection();

        $user = User::getUserByEmail($pdo, $inputEmail);

        if (!$user) {
            $_SESSION['errorMsg'] = 'Email incorrect';
            header("Location: /");
            return;
        }

        if (!password_verify($inputPassword, $user['password'])) {
            $_SESSION['errorMsg'] = 'Password incorrect';
            header("Location: /");
            return;
        }

        session_start();
        $_SESSION['username'] = $user['username'];
        $_SESSION['firstName'] = $user['firstName'];
        $_SESSION['lastName'] = $user['lastName'];
        $_SESSION['profileImageLink'] = $user['profileImageLink'];

        header("Location: /wall/");
    }

}

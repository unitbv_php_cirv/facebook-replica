<?php

namespace App\Controllers;

use App\db;
use App\Helpers\PostHelper;
use App\Models\Post;
use Framework\Controller;

class UserProfileController extends Controller
{
    public function Show($username)
    {
        $pdo = (new db())->dbConnection();
        $postHelper = new PostHelper();
        if (isset($_POST["addPost"])) {
            $_SESSION["currentPage"] = "/userProfile/" . $_SESSION["username"] . "/";
            $postHelper->AddNewPost($pdo);
        }
        elseif (isset($_POST["deletePost"])) {
            $postHelper->DeletePost($pdo, $_POST["deletePostID"]);
        }
        elseif (isset($_POST['editPost'])) {
            $postHelper->EditPost($pdo, $_POST['editPostID']);
        }

        $twigParams = [
            'inf_scroll_regex' => '{{#}}',
            'profileUsername' => $username["user"],
            "username" => $_SESSION["username"],
            "profileImageLink" => $_SESSION["profileImageLink"]
        ];
        $this->view("UserProfilePage.html", $twigParams);
    }
}
<?php

namespace App\Controllers;

use Framework\Controller;

class NotFoundController extends Controller
{
    public function notFoundAction()
    {
        http_response_code(404);
        $this->view("404.html");
    }
}

<?php

namespace App\Controllers;

use App\db;
use App\Helpers\PostHelper;
use Framework\Controller;

class WallController extends Controller
{
    public function Show()
    {
        $pdo = (new db())->dbConnection();
        $postHelper = new PostHelper();
        if (isset($_POST["addPost"])) {
            $_SESSION["currentPage"] = "/wall/";
            $postHelper->AddNewPost($pdo);
        }
        elseif (isset($_POST["deletePost"])) {
            $postHelper->DeletePost($pdo, $_POST["deletePostID"]);
        }
        elseif (isset($_POST['editPost'])) {
            $postHelper->EditPost($pdo, $_POST['editPostID']);
        }
        elseif (isset($_POST["updateLike"])) {
            $postHelper->UpdateLikeNumber($pdo, $_POST["updatePostID"], $_SESSION["username"]);
        }

        $twigParams = [
                'inf_scroll_regex' => '{{#}}',
                "username" => $_SESSION["username"],
                "profileImageLink" => $_SESSION["profileImageLink"]
            ];
            $this->view("WallPage.html", $twigParams);
        }
}

<?php
/**
 * Created by PhpStorm.
 * User: todor
 * Date: 2/3/2019
 * Time: 4:53 PM
 */

namespace App\Controllers;


use App\Helpers\ErrorTrigger;
use Framework\Controller;
use Framework\FileOperator;
use App\db;
use App\Models\User;

class SettingsController extends Controller
{
    public function Show()
    {
        if(isset($_POST["saveChanges-submit"]))
        {
            if (!isset($_POST['firstName'])
            || !isset($_POST['lastName'])) {
                $_SESSION['errorMsg'] = 'Error transmitting data.';
                header("Location: /settings/");
                return;
            }

            $pdo = (new db())->dbConnection();
            $profileImageLink = null;
            if($_FILES["profileImageToUpload"]["name"]) {
                $fileLocation = (new FileOperator())->UploadFile("profileImageToUpload");
                if (isset($fileLocation)) {
                    $profileImageLink = $fileLocation;
                }
            }
            $outcome = User::updateUserDetails($pdo, $_SESSION["username"], $_POST["firstName"], $_POST["lastName"], $profileImageLink);
            if ($outcome){
                $_SESSION["firstName"] = $_POST["firstName"];
                $_SESSION["lastName"] = $_POST["lastName"];
                if($profileImageLink != null){
                    $_SESSION["profileImageLink"] = $profileImageLink;
                }
                $_SESSION['errorMsg'] = "Details updated";
            }
            else {
                $_SESSION['errorMsg'] = "Couldn't update details :(";
                header("Location: /settings/");
                return;
            }
        }
        if(isset($_POST["changePassword-submit"]))
        {
            if (!isset($_POST['password'])
            || !isset($_POST['confirmPassword'])) {
                $_SESSION['errorMsg'] = 'Error transmitting data.';
                header("Location: /settings/");
                return;
            }

            $pdo = (new db())->dbConnection();
            if($_POST['password'] == $_POST['confirmPassword'])
            {
                User::updateUserPassword($pdo, $_SESSION["username"], $_POST["password"]);
                $_SESSION['errorMsg'] = "Password updated";
            }
            else {
                $_SESSION['errorMsg'] = "Passwords did not match";

            }
        }
        if(isset($_POST["delete-User-submit"]))
        {
            $pdo = (new db())->dbConnection();
            User::deleteUser($pdo, $_SESSION["username"]);
            header("Location: /logout/");
            session_destroy();
            return;
        }

        $twigSession = [
            "userLink" => "/userProfile/".$_SESSION["username"].'/',
            "username" => $_SESSION["username"],
            "firstName" => $_SESSION["firstName"],
            "lastName" => $_SESSION["lastName"],
            "profileImageLink" => $_SESSION["profileImageLink"]
        ];

        ErrorTrigger::add($twigSession);

        $this->view("SettingsPage.html", $twigSession);
    }
}
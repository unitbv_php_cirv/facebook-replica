<?php

namespace App\Controllers;

use App\Helpers\PostHelper;
use Framework\Controller;
use App\db;
use App\Models\Post;
use App\Models\User;
use App\Models\Like;

class GetPostController extends Controller
{
    private $pdo;
    private $postHelper;
    private $postIndex;

    public function __construct()
    {
        parent::__construct();
        $this->pdo = (new db())->dbConnection();
        $this->postHelper = new PostHelper();
    }

    public function getPost($params)
    {
        if (!$this->initPostIndex($params)) {
            return;
        }

        if (!$this->validateGetParameters()) {
            return;
        }

        $username = '';
        switch ($_GET['owner'])
        {
            case 'all':
                break;
            default:
                $username = $_GET['owner'];
        }

        $posts = Post::getPosts($this->pdo, $username);

        if (!$this->validateNbOfPosts($posts)) {
            return;
        }

        $requestedPost = $this->postHelper->ConfigurePost($this->pdo, $posts[$this->postIndex]);
        $requestedPost['originalDescription'] = $requestedPost['description'];

        $likeModel = new Like();
        $number=$likeModel->getLikeNumber($this->pdo, $requestedPost["id"]);


        $preg_callback = function ($matches)
        {
            $username = substr($matches[0], 1);
            if (User::checkUserExists($this->pdo, $username))
            {
                return "<a href=\"/userProfile/$username\">$matches[0]</a>";
            }

            return "$matches[0]";
        };

        $requestedPost["description"] = preg_replace_callback('/\B@\S+/', $preg_callback, $requestedPost["description"]);

        foreach ($requestedPost["comments"] as &$comment)
        {
            $comment["description"] = preg_replace_callback('/\B@\S+/', $preg_callback, $comment["description"]);
            $comment["likeNumber"] = (new Like())->getLikeNumber($this->pdo, $comment["id"]);
        }

        $this->view('getPost.html',
            [
                'post' => $requestedPost,
                "username" => $_SESSION["username"],
                'likeNumber' => $number

            ]);
    }

    private function initPostIndex($params)
    {
        if (!isset($params['id'])) {
            (new NotFoundController())->notFoundAction();
            return false;
        }

        $this->postIndex = intval($params['id']);

        if ($this->postIndex == 0) {
            // in this case, intval failed because infinite-scroll returns numbers > 1
            (new NotFoundController())->notFoundAction();
            return false;
        }

        $this->postIndex -= 2; // this offset is needed because infinite-scroll starts counting at 2
        return true;
    }

    private function validateGetParameters()
    {
        if (!isset($_GET['owner']) or !isset($_GET['order'])) {
            (new NotFoundController())->notFoundAction();
            return false;
        }

        switch ($_GET['order'])
        {
            case 'ichrono': // inverse chronologically = from newest to oldest
                // all of our functions in Post class return from newest to oldest
                break;
            default:
                (new NotFoundController())->notFoundAction();
                return false;
        }

        return true;
    }

    private function validateNbOfPosts($posts)
    {
        if (!count($posts)) {
            (new NotFoundController())->notFoundAction();
            return false;
        }

        if (count($posts) <= $this->postIndex) {
            echo null;
            return false;
        }
        return true;
    }

}

<?php

namespace App\Controllers;

use App\Helpers\ErrorTrigger;
use Framework\Controller;

class IndexController extends Controller
{
    public function indexAction()
    {
        $twigParams = [];
        ErrorTrigger::add($twigParams);
        $this->view("index.html", $twigParams);
    }
}

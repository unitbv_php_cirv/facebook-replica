<?php

namespace App;

use PDO;

class db
{
    function dbConnection()
    {
        $username = '';
        $password = '';
        $dbhost = '';
        $dbport = '';
        $dbname = '';
        $charset = '';

        if (Config::DEPLOYMENT == DeploymentType::LOCAL) {
            $dbhost = 'localhost';
            $dbport = '3306';
            $dbname = 'facebookreplica';
            $charset = 'utf8mb4';
            $username = 'root';
            $password = '';
        } else if (Config::DEPLOYMENT == DeploymentType::AMAZON_AWS_EB) {
            $dbhost = $_SERVER['RDS_HOSTNAME'];
            $dbport = $_SERVER['RDS_PORT'];
            $dbname = $_SERVER['RDS_DB_NAME'];
            $charset = 'utf8mb4';
            $username = $_SERVER['RDS_USERNAME'];
            $password = $_SERVER['RDS_PASSWORD'];
        }

        $dsn = "mysql:host={$dbhost};port={$dbport};dbname={$dbname};charset={$charset}";

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false
        ];

        try {
            $pdo = new PDO($dsn, $username, $password, $options);

            $count = 0;

            $prop1 = "SELECT * FROM information_schema.tables WHERE table_schema = '{$dbname}' AND table_name = 'users' LIMIT 1;";
            $prop2 = "SELECT * FROM information_schema.tables WHERE table_schema = '{$dbname}' AND table_name = 'posts' LIMIT 1;";
            $prop3 = "SELECT * FROM information_schema.tables WHERE table_schema = '{$dbname}' AND table_name = 'likes' LIMIT 1;";

            $stmt = $pdo->prepare($prop1);
            $stmt->execute();
            if ($stmt->fetch()) {
                $count++;
            }

            $stmt = $pdo->prepare($prop2);
            $stmt->execute();
            if ($stmt->fetch()) {
                $count++;
            }

            $stmt = $pdo->prepare($prop3);
            $stmt->execute();
            if ($stmt->fetch()) {
                $count++;
            }

            if ($count < 3) {
                $prop = "
CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `postId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;";
                $pdo->prepare($prop)->execute();

                $prop = "
CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `description` varchar(1000) COLLATE utf8mb4_bin NOT NULL,
  `dateTime` datetime NOT NULL,
  `pictureLink` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL,
  `postId` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;";
                $pdo->prepare($prop)->execute();

                $prop = "
CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(25) COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_bin NOT NULL,
  `firstName` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `lastName` varchar(50) COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_bin NOT NULL,
  `profileImageLink` varchar(255) COLLATE utf8mb4_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin;";
                $pdo->prepare($prop)->execute();

                $prop = "
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `likes_ibfk_1` (`postId`),
  ADD KEY `likes_ibfk_2` (`userId`);";
                $pdo->prepare($prop)->execute();

                $prop = "
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);";
                $pdo->prepare($prop)->execute();

                $prop = "
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);";
                $pdo->prepare($prop)->execute();

                $prop = "
ALTER TABLE `likes`
  ADD CONSTRAINT `likes_ibfk_1` FOREIGN KEY (`postId`) REFERENCES `posts` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `likes_ibfk_2` FOREIGN KEY (`userId`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
";

                $prop = "ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";
                $pdo->prepare($prop)->execute();

                $prop = "ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";
                $pdo->prepare($prop)->execute();

                $prop = "ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT";
                $pdo->prepare($prop)->execute();
            }

            return $pdo;
        } catch (PDOException $e) {
            throw new PDOException($e->getMessage(), $e->getCode());
        }
    }
}

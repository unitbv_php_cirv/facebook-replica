<?php

namespace Framework;

class Router
{
    private $static_routes, $dynamic_routes;

    public function __construct(array $static_routes, array $dynamic_routes)
    {
        $this->static_routes = $static_routes;
        $this->dynamic_routes = $dynamic_routes;
    }

    public function route(string $url, string $query) {
        $params = array();
        $this->getControllerName($this->static_routes, '', $controller, $action, $middleware);
        $split_query = null;

        // remove the query string from the URL and make sure the link ends with a slash
        if (isset($url)) {
            $url = preg_replace('/\?.*/', '', $url);
            $url = rtrim($url, '/') . '/';
        }

        // split the query string into an array
        if (isset($query)) {
            parse_str($query, $split_query);
        }

        // static route assignment
        if (isset($this->static_routes[$url])) {
            $this->getControllerName($this->static_routes, $url, $controller, $action, $middleware);
        }

        // route ending in a number assignment
        else if (preg_match('/:?\d+\/$/', $url, $id)) {
            $id = rtrim($id[0], '/');
            // replacement of id in $url needed first, then we can remove the : before it
            $url = str_replace($id, '{id}', $url);
            $id = ltrim($id, ':');
            $params['id'] = $id;
            $this->getControllerName($this->dynamic_routes, $url, $controller, $action, $middleware);
        }

        // route ending in a word assignment
        else if (preg_match('/:?\w+\/$/', $url, $id)) {
            $id = rtrim($id[0], '/');
            // replacement of id in $url needed first, then we can remove the : before it
            $url = str_replace($id, '{user}', $url);
            $id = ltrim($id, ':');
            $params['user'] = $id;
            $this->getControllerName($this->dynamic_routes, $url, $controller, $action, $middleware);
        }

//        // route ending in a number/detail assignment
//        else if (preg_match('/:?\d+\/\w+\/$/', $url, $matched)) {
//            /*
//             * max 2 elements in the returned explode, so everything after the first slash is
//             * forced into the second item
//             * $matched[0] = {id}
//             * $matched[1] = {detail}
//             */
//            $matched = explode('/', $matched[0], 2);
//            $id = $matched[0];
//            $detail = rtrim($matched[1], '/');
//
//            // replacement of id in $url needed first, then we can remove the : before it
//            $url = str_replace($id, '{id}', $url);
//            $url = str_replace($detail, '{detail}', $url);
//
//            $id = ltrim($id, ':');
//
//            $params['id'] = $id;
//            $params['detail'] = $detail;
//
//            $this->getControllerName($this->dynamic_routes, $url, $controller, $action, $middleware);
//        }
        if ($middleware){
            $middleware->handle(); // TODO: decide what params to send it
        }
        $controller->{$action}($params, $split_query);
    }

    private function getControllerName(array $routes, string $url, &$controller, &$action, &$middleware) {
        if (!isset($routes[$url])) {
            $url = '';
        }

        $controllerName = $routes[$url]['controller'];
        $action = $routes[$url]['action'];
        $class = "\App\Controllers\\$controllerName";

        $controller = new $class();

        $middleware = '';
        if (isset($routes[$url]['middleware'])) {
            $middlewareName = $routes[$url]['middleware'];
            $middlewareClass = "\App\Middlewares\\$middlewareName";
            $middleware = new $middlewareClass();
        }
    }
}

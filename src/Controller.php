<?php
namespace Framework;

class Controller
{
   private $twig, $s3;

   public function __construct()
   {
       $loader = new \Twig_Loader_Filesystem(__DIR__ . '/../app/Views');
       $twigEnvParams = $this->prepareTwigEnvironmentParams();
       $this->twig = new \Twig_Environment($loader, $twigEnvParams);
   }

   public function view(string $viewFile, array $params = [])
   {
       $this->twig->display($viewFile, $params);
   }

   private function prepareTwigEnvironmentParams()
   {
       $envParams['cache'] =  __DIR__ . '/../storage/cache/views';

       if(\App\Config::ENVIRONMENT === \App\EnvironmentType::DEVELOPMENT) {
           $envParams['cache'] = false;
           $envParams['debug'] = true;
       }

       return $envParams;
   }
}

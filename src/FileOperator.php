<?php

namespace Framework;

class FileOperator
{
    public function UploadFile($nameOfTheFileRegister)
    {
        $imageFileType = strtolower(pathinfo($_FILES[$nameOfTheFileRegister]["name"], PATHINFO_EXTENSION));
        $new_file_name = $this->GUID() . ".{$imageFileType}";
        $uploadOk = 1;

        $s3 = new S3Helper();

        if (isset($_POST["addPost"])) {
            $check = getimagesize($_FILES[$nameOfTheFileRegister]["tmp_name"]);
            if ($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
                return null;
            }
        }
        if ($s3->IsFileOnS3($new_file_name)) {
            echo "Sorry, file already exists.";
            return null;
            $uploadOk = 0;
        }
        if ($_FILES[$nameOfTheFileRegister]["size"] > 5000000) {
            echo "Sorry, your file is too large.";
            return null;
            $uploadOk = 0;
        }
        if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
            && $imageFileType != "gif") {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            return null;
            $uploadOk = 0;
        }
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
            return null;
        } else {
            $result = $s3->UploadFileToS3($_FILES[$nameOfTheFileRegister]["tmp_name"], $new_file_name);

            if ($result)
            {
                echo "The file " . basename($_FILES[$nameOfTheFileRegister]["name"]) . " has been uploaded.";
                return $result['ObjectURL'];
            } else {
                echo "Sorry, there was an error uploading your file.";
                return null;
            }
        }
    }

    private function GUID()
    {
        if (function_exists('com_create_guid') === true)
        {
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }
}

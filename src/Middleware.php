<?php

namespace Framework;

interface Middleware
{
    // this function will be called in the router as the "guarding" logic
    public function handle(array $params = null);

    // this function is called inside handle() if certain conditions are met
    // its purpose is to redirect the page
    function reject();

}

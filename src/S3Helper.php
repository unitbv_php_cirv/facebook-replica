<?php

namespace Framework;

use Aws\S3\Exception\S3Exception;

class S3Helper
{
    private $s3;

    public function __construct()
    {
        $this->s3 = new \Aws\S3\S3Client([
            'region' => $_SERVER['S3_REGION'],
            'version' => $_SERVER['S3_VERSION'],
            'credentials' => [
                'key'    => $_SERVER['S3_KEY'],
                'secret' => $_SERVER['S3_SECRET']
            ]
        ]);
    }

    public function UploadFileToS3($file, $name)
    {
        try {
            return $this->s3->putObject([
                'Bucket'        => $_SERVER['S3_BUCKET'],
                'Key'           => "{$name}",
                'SourceFile'    => $file,
                'ACL'           => 'public-read'
            ]);
        } catch (S3Exception $e) {
            return null;
        }
    }

    public function IsFileOnS3($fileName)
    {
        try {
            return $this->s3->getObject([
                'Bucket' => $_SERVER['S3_BUCKET'],
                'Key'    => $fileName
            ]);
        } catch (S3Exception $e) {
            return null;
        }
    }
}

function updateLabel(inputId, labelId) {

    let input = document.getElementById(inputId);
    let fileName = input.value.split('\\').pop();
    let label = document.getElementById(labelId);

    if (fileName === '') {
        label.value = "Choose a file";
    }
    else {
        label.value = fileName;
        label.textContent = fileName;
        label.width = 'auto';
        label.style.overflow = "hidden";
    }
}

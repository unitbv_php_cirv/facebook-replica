function updateHeight(postId, commentId, scrollCommentId) {

    let post = document.getElementById(postId);
    let height = post.offsetHeight;
    let comment = document.getElementById(commentId);
    let scroll = document.getElementById(scrollCommentId);

    if (height > 200) {
        comment.setAttribute("style","height:"+height+"px");
    }
    else {
        comment.setAttribute("style","height:200px");
    }

    scroll.setAttribute("style","height:"+comment.offsetHeight+"px; list-style-type:none;");
}
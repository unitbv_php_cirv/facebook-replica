function toggleEdit(postId) {

    if ($('#edit-button' + postId).hasClass('notset')) {
        $('#edit-button' + postId).removeClass('notset');


        $('#edit-button' + postId).click(function (e) {
            if ($(this).hasClass('notsmt')) {
                $(this).removeClass('notsmt');
                $(this).addClass('smt');

                $('#description' + postId).fadeOut(100);
                $('#description' + postId).removeClass('active');

                $('#edit-form' + postId).delay(100).fadeIn(100);
                $('#edit-form' + postId).addClass('active');
            } else {
                $(this).removeClass('smt');
                $(this).addClass('notsmt');

                $('#edit-form' + postId).fadeOut(100);
                $('#edit-form' + postId).removeClass('active');

                $('#description' + postId).delay(100).fadeIn(100);
                $('#description' + postId).addClass('active');
            }


            e.preventDefault();
        });
    }
}

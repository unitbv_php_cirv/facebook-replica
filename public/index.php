<?php
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../app/routes.php';

use Tracy\Debugger;
use Framework\Router;

ini_set("display_errors", 0);
Debugger::enable(Debugger::PRODUCTION);

if (\App\Config::ENVIRONMENT === \App\EnvironmentType::DEVELOPMENT) {
    error_reporting(E_ALL);
    ini_set("display_errors", 1);
    Debugger::enable(Debugger::DEVELOPMENT);
}

$router = new Router($static_routes, $dynamic_routes);
$router->route($_SERVER['REQUEST_URI'], $_SERVER['QUERY_STRING']);

# Use Cases:
    User:
        Properties:
            -Id (unique)
            -Username (unique)
            -Email (unique)
            -Name
            -Surname
            -Posts (Array of posts added by him)
            -ProfilePicture
        Actions:
            -Register (apache database)
            -LogIn
            -LogOut
            -Add Post
            -Delete Post
            -View all Pictures uploaded (by anyone so far)
# Architecture:
    Items:
        -Pictures (Array of picture links uploaded by everyone)
    MainPages:
        -LoginPage
        -RegisterPage
        -WallPage (http:replica/wall)
        -UserProfilePage (we will get it by a dynamic link e.g.:http:replica/profile/Johny)
        -SettingsPage
        -AddPostPage
    Concepts:
        -Post which contains: username of the owner, datetime that the post has been made, description, link to the aferent picture (IF NEEDED)
            *Comments and comments functionality
            *Tag users in description with @username
            *Click on the image from the post will zoom it in
            *Edit the post directly from it
        
# Pages:
    UserProfilePage:
        -Shows profile picture, name and surname, and chronologically ordered posts uploaded by the user
        -"Settings" button visible only by the owner user. This leads to SettingsPage
        -"Delete" button for every post visible only by the owner user.
    SettingsPage:
        -Edit Name, Surname, Profile picture.
    WallPage:
        -Shows chronologically ordered posts uploaded by every user.
        
   
   
(*)=If time permits